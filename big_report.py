import sqlite3
from matplotlib.pyplot import subplot
import pandas as pd
import numpy as np
import datapane as dp
import plotly_express as px
import math

def calculate_bts(df):
    df['BTS%'] = 1-abs(1-df['result'].div(df['plan']))
    df = df.replace([np.inf, -np.inf], 0)
    df['BTS%'] *= (df['BTS%']>0)

    return df

connection = sqlite3.connect('report.db')
cursor = connection.cursor()
df = pd.read_sql_query("SELECT * from reports", connection)


subpage = dp.Select(blocks=[
            dp.DataTable(df),
            dp.Text("This is a test"),
            dp.DataTable(df)
        ])

report = dp.Report(
    dp.Page(
        title="Page 1",
        blocks=[subpage]
    ),
    dp.Page(
        title="Page 2",
        blocks = ["Page 2"]
    ),
    dp.Page(
        title="Page 3",
        blocks = ["Page 3"]
    ),
)

report.save(path='test.html', open=True)
# print(df.head())
import sqlite3
import pandas as pd
import numpy as np

def calculate_bts(df):
    df['BTS%'] = 1-abs(1-df['result'].div(df['plan']))
    df = df.replace([np.inf, -np.inf], 0)
    df['BTS%'] *= (df['BTS%']>0)

    return df

def query_db():
    # Pull data from database.
    df = pd.read_sql_query("SELECT * from reports", connection)

    # Add a new column calculating the build to schedule % for each group, replacing np.inf with 0.
    # df['BTS%'] = 1-abs(1-df['result'].div(df['plan']))
    # df = df.replace([np.inf, -np.inf], 0)
    df = calculate_bts(df)

    return df

# Connect to report database
connection = sqlite3.connect('report.db')

# Creating a cursor object to query the table
cursor = connection.cursor()

df = query_db()
writer = pd.ExcelWriter('database_dump.xlsx', engine='xlsxwriter')
df.to_excel(writer, sheet_name='Report', index=False)
writer.save()
import PySimpleGUI as sg
import sqlite3
import pandas as pd
import numpy as np
import datapane as dp
import plotly_express as px
import math

def calculate_bts(df):
    df['BTS%'] = 1-abs(1-df['result'].div(df['plan']))
    df = df.replace([np.inf, -np.inf], 0)
    df['BTS%'] *= (df['BTS%']>0)

    return df

def make_report(input):
    df = pd.read_excel('712.xlsx')
    df = df[df.Shift.notnull()]
    df = df.drop(['BTS%', 'Start Time', 'End Time'], axis=1)
    df['2nd Staff'] = df['2nd Staff'].fillna(df['3rd Staff'])
    df['1st Staff'] = df['1st Staff'].fillna(df['2nd Staff'])
    df['1st Staff'] = df['1st Staff'].fillna(0)
    df['Result'] = df['Result'].fillna(0)
    df = df.rename(columns={'Shift': 'shift', 'Date': 'date', 'Work Center': 'work_center', 'Part Number': 'part_number', 'Description': 'description', 'Plan': 'plan', 'Result': 'result', '1st Staff': 'manning', 'Notes': 'notes'})
    df = df.drop(['2nd Staff', '3rd Staff'], axis=1)
    df['notes'] = df['notes'].fillna('')

    df['date'] = pd.to_datetime(df['date'])
    df_sum = df.groupby(['work_center', 'shift', 'date'], as_index=False)["plan", "result"].sum()
    df_notes = df.groupby(["work_center", 'shift', 'date'], as_index=False)["notes"].agg(lambda col: ''.join(col))

    df_grouped = df_sum.merge(df_notes, on=["work_center", 'shift', 'date'])
    df = calculate_bts(df)
    df_grouped = calculate_bts(df_grouped)

    df['BTS%'] *= 100
    df = df.round({'BTS%': 0})
    df_grouped['BTS%'] *= 100
    df_grouped = df_grouped.round({'BTS%': 0})

    btsChart = px.bar(df_grouped, x='work_center', y="BTS%", color='shift', barmode='group', text_auto=True)

    pvrChart = px.bar(df_grouped, x='work_center', y=['plan', 'result'], barmode='overlay', facet_col="shift", text_auto=True)
    for annotation in pvrChart.layout.annotations:
        annotation.text = annotation.text.split("=")[1]

    bts = math.floor(df_grouped['BTS%'].mean())

    df_date = df_grouped.iloc[0,2]

    datasetGrouped = dp.Table(df_grouped)
    datasetRaw = dp.Table(df)

    stats = dp.Group(
        dp.Text("![](logo.png)"),
        dp.BigNumber(
            heading="# Lines Achieved 100%",
            value= df_grouped['BTS%'].value_counts()[100]
        ),
        dp.BigNumber(
            heading="Average BTS%",
            value= f'{bts}%'
        ),columns=3
    )
    page1 = [stats, btsChart, pvrChart]

    report = dp.Report(
        dp.Page(
            title=f"{df_date.strftime('%m/%d')[:5]} Daily Report",
            blocks= page1 
        ),
        dp.Page(
            title="Dataset",
            blocks = [datasetGrouped]
        ),
        dp.Page(
            title="Raw Dataset",
            blocks = [datasetRaw]
        )
    )
    report_date = df_date.strftime('%m-%d')[:5]
    report.save(path=f'{report_date} Daily Report.html')

sg.theme('BluePurple')

layout = [[sg.Text('File to generate report from:'), sg.Text(size=(15,1), key='-OUTPUT-')],
          [sg.Input(key='-IN-'), sg.FileBrowse()],
          [sg.Button('Generate'), sg.Button('Exit')]]

window = sg.Window('Pattern 2B', layout)

while True:  # Event Loop
    event, values = window.read()
    print(event, values)
    if event == sg.WIN_CLOSED or event == 'Exit':
        break
    if event == 'Generate':
        # Update the "output" text element to be the value of "input" element
        make_report('-IN-')
        sg.popup('Complete!')

window.close()

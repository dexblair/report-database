#!/usr/bin/env python

# Imports
import pandas as pd
import csv
import sqlite3
import argparse
import numpy as np
import os
import glob
from pkg_resources import working_set
import datapane as dp

#---------Initialization---------#
# Connect to report database
connection = sqlite3.connect('report.db')
# Creating a cursor object to query the table
cursor = connection.cursor()
#---------Initialization---------#

#---------Function Definitions---------#
def create_table():
    # Drops the existing reports table.
    cursor.execute('''DROP TABLE reports''')

    # Table Definition
    create_table = '''CREATE TABLE if not exists reports(
                shift TEXT NOT NULL,
                date TEXT NOT NULL,
                work_center TEXT NOT NULL,
                part_number TEXT NOT NULL,
                description TEXT NOT NULL,
                plan INTEGER NOT NULL,
                result INTEGER NOT NULL,
                manning INTEGER,
                notes TEXT);
                '''
    cursor.execute(create_table)
    
# Helper function to read a csv file and return its contents.
def read_file(filename):  
    file = open(filename)
    return csv.reader(file)

# Defines our SQL INSERT query, takes an open csv file as input, and executes our query with the contents.
def add_report(report):
    contents = read_file(report)
    insert_records = "INSERT INTO reports (shift, date, work_center, part_number, description, plan, result, manning, notes) VALUES(?,?,?,?,?,?,?,?,?)"
    # Run the SQL query.
    cursor.executemany(insert_records, contents)

# Iterates over a fixed input directory and calls add_report on each file.
def add_reports():
    for file in glob.glob("csv/*.csv"):
        add_report(file)

def calculate_bts(df):
    df['BTS%'] = 1-abs(1-df['result'].div(df['plan']))
    df = df.replace([np.inf, -np.inf], 0)
    df['BTS%'] *= (df['BTS%']>0)

    return df
    
# Creates source dataframe by querying sqlite db and populating calculated fields.
def query_db():
    # Pull data from database.
    df = pd.read_sql_query("SELECT * from reports", connection)

    # Add a new column calculating the build to schedule % for each group, replacing np.inf with 0.
    # df['BTS%'] = 1-abs(1-df['result'].div(df['plan']))
    # df = df.replace([np.inf, -np.inf], 0)
    df = calculate_bts(df)

    return df

# Formats excel report for a specific date.
def write_report_for_date(report_date):
    # Retrieve data from SQL
    df = query_db()

    # Filter dataframe by supplied date.
    df = df[df['date'] == report_date]

    # Output filtered dataframe to spreadsheet.
    writer = pd.ExcelWriter('report.xlsx', engine='xlsxwriter')
    df.to_excel(writer, sheet_name='Report', index=False)
    writer.save()

# Formats excel report for entire dataset.
def write_full_report():
    # Pull data from database.
    df = query_db()

    # Group data by work center, shift, and date to summarize orders. Do the same to collect notes.
    dfsum = df.groupby(["work_center", 'shift', 'date'], as_index=False)["plan", "result"].sum()
    dfnote = df.groupby(["work_center", 'shift', 'date'], as_index=False)["notes"].agg(lambda col: ''.join(col))

    # Reset indexes and merge our aggregated dataframes.
    dfnote.reset_index()
    dfsum.reset_index()
    df_final = dfsum.merge(dfnote, on=["work_center", 'shift', 'date'])
    df_final = calculate_bts(df_final)

    # Output the final dataframe to an excel file.
    writer = pd.ExcelWriter('test.xlsx', engine='xlsxwriter')
    df_final.to_excel(writer, sheet_name='Report', index=False)
    writer.save()

def justin_report():
    df = query_db()

    dfsum = df.groupby(["work_center", 'date', 'shift'], as_index=False)["plan", "result"].sum()
    dfsum = dfsum[(dfsum["work_center"] == 'G13004') | (dfsum["work_center"] == 'G11144')]

    dfpartnum = df[(df["work_center"] == 'G13004') | (df["work_center"] == 'G11144')]

    # Output the final dataframe to an excel file.
    writer = pd.ExcelWriter('justin_report.xlsx', engine='xlsxwriter')
    # writernum = pd.ExcelWriter('detail_report.xlsx', engine='xlsxwriter')
    dfsum.to_excel(writer, sheet_name='Sum Report', index=False)
    dfpartnum.to_excel(writer, sheet_name='Detail Report', index=False)
    # writernum.save()
    writer.save()

# Development for Datapane rich reporting.
def dp_report():
    # Pull data from database.
    df = query_db()

    # Group data by work center, shift, and date to summarize orders. Do the same to collect notes.
    dfsum = df.groupby(["work_center", 'shift', 'date'], as_index=False)["plan", "result"].sum()
    dfnote = df.groupby(["work_center", 'shift', 'date'], as_index=False)["notes"].agg(lambda col: ''.join(col))

    # Reset indexes and merge our aggregated dataframes.
    dfnote.reset_index()
    dfsum.reset_index()
    df_final = dfsum.merge(dfnote, on=["work_center", 'shift', 'date'])

    report = dp.Report(
        dp.Table(df_final)
    )
    report.save(path='report.html', open=True)

# Formats excel report for given date range for BTS award.
def award_report():
    # Pull data from database.
    df = query_db()

    # Specify the date range to filter for. TODO: Refactor this to take input.
    report_range = ['6/20/2022', '6/21/2022', '6/22/2022', '6/23/2022', '6/24/2022']
    df = df[df['date'].isin(report_range)]

    # Build dataframe to check 100% bts across the period, then capture only the work centers and shifts.
    dffilter = df.groupby(["work_center", 'shift'], as_index=False)["plan", "result"].sum()
    dffilter['BTS%'] = dffilter['result'].div(dffilter['plan']).replace(np.inf, 0)
    dffilter = dffilter[dffilter['BTS%'] == 1]
    df_key = dffilter.loc[:, ["work_center", "shift"]]

    # Build dataframe for all workcenters from the period and then filter by our key dataframe.
    dfsum = df.groupby(["work_center", 'shift', 'date'], as_index=False)["plan", "result"].sum()
    dfsum['BTS%'] = dfsum['result'].div(dfsum['plan']).replace(np.inf, 0)
    dfsum.reset_index()
    df_final = dfsum.merge(df_key, how="inner", on=["work_center", "shift"])

    # Output the final dataframe to an excel file.
    writer = pd.ExcelWriter('awardtest.xlsx', engine='xlsxwriter')
    df_final.to_excel(writer, sheet_name='Report', index=False)
    writer.save()
#---------Function Definitions---------#

#---------Argument Definitions---------#
parser = argparse.ArgumentParser(description="test")
parser.add_argument("-c", "--Create", action="store_true", help="Flag to recreate table.")
parser.add_argument("-a", "--Add", help="Takes filepath for single report to add.")
parser.add_argument("-d", "--AddDirectory", action="store_true", help="Takes filepath for directory of reports to add.")
parser.add_argument("-D", "--Date", help="Date to generate report from.")
parser.add_argument("-f", "--FullReport", action="store_true", help="Returns a summary of the full database by workcenter, shift, and date.")
parser.add_argument("-w", "--Award", action="store_true", help="Returns the set of lines that achieved 100% BTS over the specified date range.")
parser.add_argument("-x", "--Datapane", action="store_true")
args = parser.parse_args()
#---------Argument Definitions---------#

#---------CLI Logic---------#
if args.Create:
    create_table()
if args.Add:
    add_report(args.Add)
if args.AddDirectory:
    add_reports()
if args.Date:
    write_report_for_date(args.Date)
if args.FullReport:
    write_full_report()
if args.Award:
    award_report()
if args.Datapane:
    justin_report()
    
#---------CLI Logic---------#

#---------Clean Up---------#
# Commit changes to database and close connection.
connection.commit()
connection.close()
#---------Clean Up---------#
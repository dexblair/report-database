import sqlite3
import pandas as pd
from pandas.tseries.offsets import BDay
import datapane as dp
import numpy as np
import math
import datetime
import plotly_express as px
# Connect to report database
connection = sqlite3.connect('report.db')

# Creating a cursor object to query the table
cursor = connection.cursor()


source = pd.read_sql_query("SELECT * from reports", connection)
source['date'] = pd.to_datetime(source['date'])
# yesterday = (datetime.datetime.today() - BDay(1))

def calculate_bts(df):
    df['BTS%'] = 1-abs(1-df['result'].div(df['plan']))
    df = df.replace([np.inf, -np.inf], 0)
    df['BTS%'] *= (df['BTS%']>0)

    return df

source_raw = source

source_sum = source.groupby(['work_center', 'shift', 'date'], as_index=False)["plan", "result"].sum()
source_notes = source.groupby(["work_center", 'shift', 'date'], as_index=False)["notes"].agg(lambda col: ''.join(col))

source = source_sum.merge(source_notes, on=["work_center", 'shift', 'date'])
source = calculate_bts(source)
source_raw = calculate_bts(source_raw)
# source['shift'] = source['shift'].str.lower()

# print(source.head())


# print(yesterday)
# work_centers = source['work_center'].unique()
bts = math.floor(source['BTS%'].mean())
source['BTS%'] *= 100
source = source.round({'BTS%': 0})
source_raw['BTS%'] *= 100
source_raw = source_raw.round({'BTS%': 0})
# source = source[source['date'] == yesterday.strftime('%Y-%m-%d')].reset_index()

df_list = [y for x, y in source.groupby('date', as_index=False)]
# print(df_list)

for source in df_list:
    
    source_local = source
    source_local = source_local.reset_index()
    source_raw_date = source_raw[source_raw['date'] == source.iloc[0,2]]
    source_raw_date = source_raw_date.reset_index()
    source_raw_date = source_raw_date.drop('index', axis = 1)
    source_local = source_local.drop('index', axis = 1)
    source_local['date'] = pd.to_datetime(source_local['date'])
    source_date = source_local.iloc[0,2]
    
    # print(source_date)

    btsChart = px.bar(source_local, x='work_center', y="BTS%", color='shift', barmode='group', text_auto=True)

    pvrChart = px.bar(source_local, x='work_center', y=['plan', 'result'], barmode='overlay', facet_col="shift", text_auto=True)
    for annotation in pvrChart.layout.annotations:
        annotation.text = annotation.text.split("=")[1]

    bts = math.floor(source_local['BTS%'].mean())
    
    dataset = dp.Table(source_local)
    dataset_raw = dp.Table(source_raw_date)
    stats = dp.Group(
        dp.Text("![](logo.png)"),
        dp.BigNumber(
            heading="# Lines Achieved 100%",
            value= source['BTS%'].value_counts()[100]
        ),
        dp.BigNumber(
            heading="Average BTS%",
            value= f'{bts}%'
        ),columns=3
    )
    page1 = [stats, btsChart, pvrChart]

    report = dp.Report(
        dp.Page(
            title=f"{source_date.strftime('%m/%d')[:5]} Daily Report",
            blocks= page1 
        ),
        dp.Page(
            title="Dataset",
            blocks = [dataset]
            
        ),
        dp.Page(
            title="Raw Dataset",
            blocks = [dataset_raw]
        )  
    )
    report_date = source_date.strftime('%m-%d')[:5]
    report.save(path=f'{report_date} Daily Report.html')
# formatting=dp.ReportFormatting(width=dp.ReportWidth.FULL)